enableSaving [false, false];
sleep 0.5;

private["_h"];
_h = [] execVM "jni.sqf";
waitUntil {scriptDone _h};

private["_date"];
_date = ["date", "yyyy/MM/dd HH:mm:ss"] call invoke_java_method;
player groupChat format["_date = %1", _date];